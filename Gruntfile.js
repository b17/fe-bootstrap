module.exports = function (grunt) {
    grunt.initConfig({
        jade: {
            compile: {
                files: [{
                    cwd: 'src',
                    src: ['**/*.jade', '!partials/**/*.jade', '!layouts/**/*.jade'],
                    dest: 'dist',
                    expand: true,
                    ext: '.html',
                }]
            },
            options: {
                pretty: true,
            }
        },
        stylus: {
            compile: {
                files: [{
                    cwd: 'src/css',
                    src: '**/*.styl',
                    dest: 'dist/css',
                    expand: true,
                    ext: '.css',
                }]
            }
        },
        coffee: {
            all: {
                expand: true,
                bare: true,
                cwd: 'src/js',
                src: ['*.coffee'],
                dest: 'dist/js',
                ext: '.js'
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dist/img',
                }]
            }
        },
        copy: {
            jquery: {
                files: [{
                    src: 'bower_components/jquery/dist/jquery.js',
                    dest: 'dist/js/jquery.js'
                }]
            },
            bootstrap: {
                files: [
                    {
                        src: 'bower_components/bootstrap/dist/js/bootstrap.js',
                        dest: 'dist/js/bootstrap.js'
                    },
                    {
                        src: 'bower_components/bootstrap/dist/css/bootstrap.css',
                        dest: 'dist/css/bootstrap.css'
                    }
                ]
            },
            css: {
                files: [{
                    cwd: 'src/css',
                    src: ['**/*.css'],
                    dest: 'dist/css',
                    expand: true,
                }]
            },
            js: {
                files: [{
                    cwd: 'src/js',
                    src: ['**/*.js'],
                    dest: 'dist/js',
                    expand: true,
                }]
            },
        },
        watch: {
            livereload: {
                options: {
                    livereload: true
                },
                files: ['dist/**/*'],
            },
            js: {
                files: ['src/js/**/*.js'],
                tasks: ['copy:js'],
            },
            coffee: {
                files: ['src/js/**/*.coffee'],
                tasks: ['coffee:all'],
            },
            css: {
                files: ['src/css/**/*.css'],
                tasks: ['copy:css'],
            },
            jade: {
                files: ['src/**/*.jade', '!partials/**/*.jade'],
                tasks: ['jade'],
            },
            stylus: {
                files: ['src/css/**/*.styl'],
                tasks: ['stylus'],
            },
            imagemin: {
                files: ['src/img/**/*.{png,jpg,gif}'],
                tasks: ['imagemin'],
            }
        },
        connect: {
            server: {
                options: {
                    port: 3000,
                    base: 'dist',
                }
            }
        },
    });

    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-coffee');

    grunt.registerTask('default', [
        'connect',
        'copy',
        'jade',
        'stylus',
        'imagemin',
        'watch',
    ]);
};
